import { stringToRegex } from './index';

describe('stringToRegex', () => {
  test('function to be defined', () => {
    expect(stringToRegex()('')).toBeDefined();
  });
  test('no options', () => {
    expect(stringToRegex()('résume')).toStrictEqual(
      /r[EeÈèÉéÊêËë]s[UuÙùÚúÛûÜü]m[EeÈèÉéÊêËë]/i
    );
  });
  test('flags empty', () => {
    expect(stringToRegex({ flags: '' })('résume')).toStrictEqual(
      /r[EeÈèÉéÊêËë]s[UuÙùÚúÛûÜü]m[EeÈèÉéÊêËë]/
    );
  });
  test('flags set', () => {
    expect(stringToRegex({ flags: 'mu' })('résume')).toStrictEqual(
      /r[EeÈèÉéÊêËë]s[UuÙùÚúÛûÜü]m[EeÈèÉéÊêËë]/mu
    );
  });
  test('multiline, unicode true, case insensitive false', () => {
    expect(
      stringToRegex({
        multilines: true,
        unicode: true,
        global: true,
        caseinsensitive: false,
      })('résume')
    ).toStrictEqual(/r[EeÈèÉéÊêËë]s[UuÙùÚúÛûÜü]m[EeÈèÉéÊêËë]/mu);
  });
  test('case insensitive true', () => {
    expect(stringToRegex({ caseinsensitive: true })('résume')).toStrictEqual(
      /r[EeÈèÉéÊêËë]s[UuÙùÚúÛûÜü]m[EeÈèÉéÊêËë]/i
    );
  });
  test('strong', () => {
    expect(
      stringToRegex({ strong: true })('la fête de machin-chose')
    ).toStrictEqual(
      /[LlĹĺĻļĽľĿŀŁłḶḷḸḹḺḻḼḽ][AaÀàÁáÂâÃãÄäÅåĀāĂăĄąǍǎǞǟǠǡǺǻȀȁȂȃȦȧȺḀḁẚẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặ] [FfḞḟ][EeÈèÉéÊêËëĒēĔĕĖėĘęĚěȄȅȆȇȨȩḔḕḖḗḘḙḚḛḜḝẸẹẺẻẼẽẾếỀềỂểỄễỆệ][TtŢţŤťŦŧȚțṪṫṬṭṮṯṰṱẗ][EeÈèÉéÊêËëĒēĔĕĖėĘęĚěȄȅȆȇȨȩḔḕḖḗḘḙḚḛḜḝẸẹẺẻẼẽẾếỀềỂểỄễỆệ] [DdĎďĐđḊḋḌḍḎḏḐḑḒḓ][EeÈèÉéÊêËëĒēĔĕĖėĘęĚěȄȅȆȇȨȩḔḕḖḗḘḙḚḛḜḝẸẹẺẻẼẽẾếỀềỂểỄễỆệ] [MmḾḿṀṁṂṃ][AaÀàÁáÂâÃãÄäÅåĀāĂăĄąǍǎǞǟǠǡǺǻȀȁȂȃȦȧȺḀḁẚẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặ][CcÇçĆćĈĉĊċČčḈḉ][HhĤĥĦħȞȟḢḣḤḥḦḧḨḩḪḫẖ][IiÌìÍíÎîÏïĨĩĪīĬĭĮįİıǏǐȈȉȊȋḬḭḮḯỈỉỊị][NnÑñŃńŅņŇňŉŊŋǸǹṄṅṆṇṈṉṊṋ]-[CcÇçĆćĈĉĊċČčḈḉ][HhĤĥĦħȞȟḢḣḤḥḦḧḨḩḪḫẖ][OoÒòÓóÔôÕõÖöØøŌōŎŏŐőǑǒǪǫǬǭǾǿȌȍȎȏȪȫȬȭȮȯȰȱṌṍṎṏṐṑṒṓỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợ][SsŚśŜŝŞşŠšȘșṠṡṢṣṤṥṦṧṨṩ][EeÈèÉéÊêËëĒēĔĕĖėĘęĚěȄȅȆȇȨȩḔḕḖḗḘḙḚḛḜḝẸẹẺẻẼẽẾếỀềỂểỄễỆệ]/i
    );
  });
  test('spaces', () => {
    expect(
      stringToRegex({ spaces: true })('la fête de machin-chose')
    ).toStrictEqual(
      /l(?:\s|'|-)*[AaÀàÁáÂâÃãÄäåĂă](?:\s|'|-)*f(?:\s|'|-)*[EeÈèÉéÊêËë](?:\s|'|-)*t(?:\s|'|-)*[EeÈèÉéÊêËë](?:\s|'|-)*d(?:\s|'|-)*[EeÈèÉéÊêËë](?:\s|'|-)*m(?:\s|'|-)*[AaÀàÁáÂâÃãÄäåĂă](?:\s|'|-)*[CcÇç](?:\s|'|-)*h(?:\s|'|-)*[IiÌìÍíÎîÏï](?:\s|'|-)*[NnÑñ](?:\s|'|-)*-(?:\s|'|-)*[CcÇç](?:\s|'|-)*h(?:\s|'|-)*[OoÒòÓóÔôÕõÖö](?:\s|'|-)*s(?:\s|'|-)*[EeÈèÉéÊêËë]/i
    );
  });
  test('mappings', () => {
    expect(
      stringToRegex({ mappings: { e: 'e', o: 'o' } })('tests over')
    ).toStrictEqual(/tests over/i);
  });
});
