'use strict';
import { Mappings, classicCharCodes, fullCharCodes } from './mappings';

/**
 * overwrite or pass your own mappings.
 * existing mappings will be overwritten, else it'll be appended to defaults
 */
export interface StringOptions {
  mappings?: Mappings;
}

/**
 * RegExp flags, ium. Defaults to i
 */
export interface RegexOptions extends StringOptions {
  flags?: string;
  strong?: boolean;
  spaces?: boolean;
  multilines?: boolean;
  unicode?: boolean;
  caseinsensitive?: boolean;
  global?: boolean;
}

function mergeMappings(
  innerMappings?: Mappings,
  strongMapping?: boolean
): Mappings {
  const base: Mappings = {};
  const codes = !strongMapping ? classicCharCodes : fullCharCodes;
  for (const code in codes) {
    base[code] = codes[code];
  }
  if (innerMappings) {
    for (const mapping in innerMappings) {
      base[mapping] = innerMappings[mapping];
    }
  }
  return base;
}

function replacer(input: string, mappings: Mappings): string {
  return input
    .split('')
    .map((letter: string) => {
      for (const mapping in mappings) {
        if (
          mapping &&
          mapping !== mappings[mapping] &&
          (mapping === letter || mappings[mapping].indexOf(letter) !== -1)
        ) {
          letter = Array.isArray(mappings[mapping])
            ? (<string[]>mappings[mapping]).join('')
            : `[${mappings[mapping]}]`;
          break;
        }
      }
      return letter;
    })
    .join('');
}

function optionsToFlag(options: RegexOptions): string {
  if (
    typeof options.flags === 'string' ||
    options.caseinsensitive ||
    options.global ||
    options.multilines ||
    options.unicode
  ) {
    let flags = ['i'];
    if (typeof options.flags === 'string') {
      flags = [''];
      options.caseinsensitive = options.global = options.multilines = options.unicode = false;
      flags.push(options.flags);
    }
    if (options.multilines === true) {
      flags.push('m');
    }
    if (options.unicode === true) {
      flags.push('u');
    }
    if (options.caseinsensitive === true) {
      flags.push('i');
    } else {
      const index = flags.indexOf('i');
      if (index > -1) {
        flags.splice(index, 1);
      } else {
        flags;
      }
    }
    const set = new Set(flags);
    flags = [...set];
    return flags.join('');
  } else {
    return 'i';
  }
}

/** Generate a function that returns a RegExp, that can be reused with the same options */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function stringToRegex(options: RegexOptions = {}) {
  const innerMappings = mergeMappings(
    typeof options.mappings === 'object' ? options.mappings : null,
    options.strong
  );
  return (input: string): RegExp => {
    if (options.spaces) {
      input = input.replace(/\s/g, '').split('').join("(?:\\s|'|-)*");
    }
    return new RegExp(
      replacer(input, innerMappings).replace(
        ':\\[SsŚśŜŝŞşŠšȘșṠṡṢṣṤṥṦṧṨṩ]',
        ':\\s'
      ),
      optionsToFlag(options)
    );
  };
}
